# Resource Monitoring
Sumber daya dari server ataupun komputer perlu dimonitor untuk dapat menghindari atau mengatasi kendala-kendala yang mengganggu program-program di dalamnya.

## 🐴 Process
### Cek Proses Yang Sedang Berjalan
Perintah **ps** dengan argumen -aux akan memberikan kita daftar proses yang sedang berjalan beserta detail dari setiap prosesnya seperti Process Identification Number (PID), penggunaan resource, serta perintah inisiasinya. PID dapat digunakan untuk manajemen proses seperti menghentikan proses berdasarkan PID nya.
```sh
ps -aux
```
Untuk melihat deskripsi penggunaan **ps**, dia bisa ngapain aja ya, gunakan argumen --help all seperti di bawah ini:
```sh
ps --help all
```

### Menghentikan Proses Yang Sedang Berjalan
Perintah **kill** digunakan untuk menghentikan proses yang sedang berjalan berdasarkan PID nya.
```
kill <PID>
```

## 🐉 Sistem Operasi
### Cek Sistem Operasi
File /etc/os-release berisi informasi sistem operasi yang ditulis oleh vendor sistem operasi tersebut.
```sh
cat /etc/os-release
```

## 🦊 RAM (Random Access Memory)
### Cek Status Penggunaan RAM
Perintah **free** digunakan untuk mengecek kondisi RAM total, yang sedang digunakan, yang tidak digunakan, dan yang digunakan sebagai swap.
```sh
free
```
Untuk melihat deskripsi penggunaan **free**, dia bisa ngapain aja ya, gunakan argumen --help seperti di bawah ini:
```sh
free --help
```

## 🐳 Hardisk
### Cek Statistik Kapasitas Hardisk
Program df / disk filesystem [📖](https://www.javatpoint.com/linux-df) merupakan program command line untuk cek statistik dari disk. 
```sh
df -h
```
Untuk melihat deskripsi penggunaan **df**, dia bisa ngapain aja ya, gunakan argumen --help seperti di bawah ini:
```sh
df --help
```

## 🦏 Network
### Cek Port yang Sedang Digunakan
Beberapa program menggunakan port sebagai jalur komunikasinya dengan eksternal. Netstat adalah program command line untuk cek aktivitas network.
```sh
netstat -tulpn | grep LISTEN
```
Untuk melihat deskripsi penggunaan **netstat**, dia bisa ngapain aja ya, gunakan argumen --help seperti di bawah ini:
```sh
netstat --help
```

### Cek Konektivitas ke Alamat Tertentu
Ping merupakan program command line untuk mengecek konektivitas ke alamat tertentu.
```sh
ping twitter.com
```
Untuk melihat deskripsi penggunaan **ping**, dia bisa ngapain aja ya, gunakan argumen --help seperti di bawah ini:
```sh
ping --help
```


## Referensi
- [Program Definition - Linfo.org](http://www.linfo.org/program.html#)
- [Linux Cheat Sheets - Cheatography](https://cheatography.com/davechild/cheat-sheets/linux-command-line/)
- [Check Running Process in Linux - Cyberciti](https://www.cyberciti.biz/faq/how-to-check-running-process-in-linux-using-command-line/)

## Challenge
1. Menyelesaikan maze game !
2. Mencoba setiap perintah terkait resource monitoring, buka --help nya, lalu pilih dan ceritakan argumen-argumen yang menurutmu menarik untuk digunakan !

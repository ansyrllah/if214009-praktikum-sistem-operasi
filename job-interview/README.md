# Job Interview

1. Mampu mendemonstrasikan penggunaan sistem operasi untuk:
    - Monitoring resource dari komputer
        - RAM
        - CPU
        - Hardisk
    - Manajemen program
        - Mampu memonitor program yang sedang berjalan
        - Mampu menghentikan program yang sedang berjalan
        - Otomasi perintah dengaan shell script
        - Penjadwalan eksekusi program dengan cron
    - Manajemen network
        - Mengakses sistem operasi pada jaringan menggunakan SSH
        - Mampu memonitor program yang menggunakan network
        - Mampu mengoperasikan HTTP client
    - Manajemen file dan folder
        - Navigasi
        - CRUD file dan folder
            - Editor
            - Otomasi
        - Manajemen ownership dan akses file dan folder
        - Pencarian file dan folder
        - Kompresi data
2. Mampu membuat program berbasis CLI menggunakan shell script
3. Mendemonstrasikan program CLI yang dibuat kepada publik dalam bentuk Youtube

# No 1
Contoh deskripsi jawaban no 1, screenshot dan screenrecord diupload di gitlab dan ditampilkan disini. Untuk mendapatkan link dari Heading No 1 ini, lihat gif dibawah. Screenrecord (perekaman layar) dalam bentuk gif dapat dilakukan menggunakan [ScreenToGif - open source](https://www.screentogif.com/) untuk Windows, atau [Peek - open source](https://github.com/phw/peek) untuk Linux.
![cara-ambil-link-heading](https://gitlab.com/marchgis/march-ed/2023/courses/if214009-praktikum-sistem-operasi/-/raw/main/job-interview/ambil-link-heading.gif)
![cara-menampilkan-image](https://gitlab.com/marchgis/march-ed/2023/courses/if214009-praktikum-sistem-operasi/-/raw/main/job-interview/menampilkan-image.gif)
# No 2
Contoh deskripsi jawaban no 2, masukan deskripsi, keterangan tambahan, link terkait, dan keterangan lainnya
# No 3
Contoh deskripsi jawaban no 3

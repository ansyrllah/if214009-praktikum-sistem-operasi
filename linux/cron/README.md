# Cron Job

Cron Job merupakan penjadwalan perintah secara periodik. Program crontab digunakan untuk menambahkan perintah penjadwalan berdasarkan aturan periode waktu tertentu, seperti per menit, per jam, per hari, dsb. Aturan periode cron job dapat dilihat [di sini](https://crontab.guru/examples.html)


## Pembuatan Cron Job via Crontab

### Edit crontab
```sh
crontab -e
```

### Tambahkan perintah cron job

Contoh menambahkan perintah touch /home/praktikumc/insan/apartment yang dieksekusi setiap menit (* * * * *)
```sh
* * * * * touch /home/praktikumc/insan/apartment > /home/praktikumc/cron.log
```

Save dan quit dari file

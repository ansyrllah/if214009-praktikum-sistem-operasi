# Linux

## Hello Operating System

### File Management

Pindah working directory ke home dengan **cd**
```sh
cd
```

Membuat directory
```sh
mkdir buang-sampah-pada-tempatnya 
```

Melihat list file dan folder pada working directory
```sh
ls .
```

Pindah working directory
```sh
cd ./buang-sampah-pada-tempatnya
```

Mengubah nama file atau folder
```sh
mv nama-folder-sebelum nama-folder-sesudah
```

Membuat file dengan **touch**
```sh
touch lemari
```

Mengetahui lokasi saat ini **working directory**
```sh
pwd
```

Menghapus file 
```sh
rm nama-filenya
```

Menulis ke file (mengubah seluruh isi file) dengan **echo**
```sh
echo "siomay tahu paria" > basotahu.food
```

Menulis ke file dengan new line **\n**
```sh
echo $'kutanya malam\ndapatkah kau lihat' > ada-apa.lagu
```

Melihat isi file dengan **cat**
```sh
cat ada-apa.lagu
```

Menghapus folder dan seluruh isinya (hati-hati!) 
```sh
rm nama-folder -rf
```

## File & Folder Mode and Ownership

### File & Folder Mode

Lihat dalam folder saat ini ada file dan folder apa saja
```sh
ls -l
```

Lalu lihat di sebelah kanan dari tiap file
```sh
---------- ini menggambarkan permission / ijin dari file tersebut

-rw-r--r-- 1 insan insan 0 Nov 24 04:49 brankas

Karakter ke 1 : jenis filenya
Karakter 2 - 4 : ijin untuk pemilik file (2 Read, 3 Write, 4 Execute)
Karakter 5 - 7 : ijin untuk grup (5 Read, 6 Write, 7 Execute)
Karakter 8 - 10 : ijin untuk seluruh pengguna (8 Read, 9 Write, 10 Execute)

Dari karakter ke 2 - 10:
  tanda - artinya tidak diijinkan
  tanda r / w / x artinya diijinkan Read / Write / Execute
```

Urutan Karakter | Tidak Diijinkan | Diijinkan | Deskripsi
--- | --- | --- | ---
1 | | | Tipe file
2 | - | r | Pemilik diijinkan Read
3 | - | w | Pemilik diijinkan Write
4 | - | x | Pemilik diijinkan Executer
5 | - | r | Grup diijinkan Read
6 | - | w | Grup diijinkan Write
7 | - | x | Grup diijinkan Executer
8 | - | r | Dunia diijinkan Read
9 | - | w | Dunia diijinkan Write
10 | - | x | Dunia diijinkan Executer


### Urutan angka pada parameter chmod

```sh
chmod 000 ./nama-file
```

Urutan Angka | User
--- | --- 
1 | Pemilik (Owner)
2 | Grup
3 | World

### Komponen angka pada  parameter chmod
Mode | Deskripsi | Nilai Angka
--- | --- | ---
Read | Membaca isi file | 4
Write | Menulis isi file | 2
Execute | Menjalankan / mengeksekusi perintah-perintah di dalam file | 1

Contoh penggunaan chmod berdasarkan tabel nilai mode di atas
```
Owner bisa Read Write dan Execute = 4 + 2 + 1 = 7
Group bisa Read dan Execute = 4 + 1 = 5
World tidak bisa akses apapun = 0
chmod 750 ./nama-file
```

```
Owner bisa Read dan Write = 4 + 2 = 6
Group bisa Read = 4
World bisa Read = 4
chmod 644 ./nama-file
```

### File Ownership
File Ownership merepresentasikan kepemilikan owner atau group terhadap file ataupun folder. Ketika melihat detail isi folder dengan perintah **ls -l**, maka akan tampil informasi dengan format:

\-  | Mode dari file | Jumlah [Hard Link](https://unix.stackexchange.com/questions/43046/what-is-the-number-between-file-permission-and-owner-in-ls-l-command-output#answer-43047) | Owner | Group | Size | Waktu modifikasi terakhir | Nama file
--- | --- | --- | --- | --- | --- | --- | ---
**Contoh** | drwxrwxr-x | 4 | research | research | 4096 | Apr 14 07\:41 | makanan

Pada contoh di atas direktori **makanan** dimiliki oleh owner bernama **research** dan group bernama **research**. Hal ini terkait dengan File Mode yang dijelaskan di bagian sebelumnya. 

- Owner : User pemilik dari file / folder tersebut
- Group : Group pemilik dari file / folder tersebut

Mengubah ownership dari sebuah file atau folder dilakukan dengan perintah **chown** <NAMA_OWNER>:<NAMA_GROUP> <NAMA_FILE_ATAU_FOLDER>

Mengubah owner dari folder. Arti perintah di bawah ini, folder tambang-mahal ownershipnya diubah menjadi **negara** dan group ownershipnya diubah menjadi **rakyat**
```sh
chown negara:rakyat tambang-mahal
```


## Aneka perintah
### Download file
```sh
wget https://archives.bulbagarden.net/media/upload/thumb/f/fb/0001Bulbasaur.png/120px-0001Bulbasaur.png
```

### Cara Menggunakan VIM
Perintah | Deskripsi
--- | ---
Klik tombol [Esc] | Masuk ke mode VISUAL
Klik tombol [i] | Masuk ke mode INSERT
Dari mode VISUAL, ketik :w lalu tekan [Enter] | Save tulisan
Dari mode VISUAL, ketik :q lalu tekan [Enter] | Keluar dari VIM


## Challenge
1. Buat struktur folder seperti di bawah ini:
```
kampus
- fakultas a
  - jurusan a1
    - mahasiswa
      - mahasiswa m1
        - tugas
          - tugas-1.txt : permission 775
          - tugas-2.txt : permission 750
      - mahasiswa m2
      - mahasiswa m3
  - jurusan a2
  - jurusan a3
- fakultas b
- fakultas c
```

2. Buat game role play berbasis folder
contoh file html:
```html
ini adalah sebuah lapangan bola
<a href="namaweb.com/maze/dadan/lapang-bola/rumah-kosong.html">rumah kosong</a>
```

# Sistem Operasi

## Definisi
- Software
- Menyediakan interface untuk berinteraksi dengan hardware
    - Interface bagi user
    - Interface bagi program

## Fungsi
- Manajemen Process
- Manajemen Memory
- Manajemen File
- Manajemen Device
- Manajemen I/O
- Manajemen Network
- Menginterpretasi Command
